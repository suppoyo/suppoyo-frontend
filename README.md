# Suppoyo: Frontend

[Suppoyo](https://suppoyo.de/) ist eine Anwendung, die Hilfsbedürftigen und Kranken helfen soll, ihre Einkäufe zu erledigen. Hierzu verbindet man Hilfsbedürftige mit Helfern, die dann die Einkäufe erledigen kann. Dieses Projekt ist im Rahmen des [WirVsVirus Hackathon](https://wirvsvirushackathon.org/) entstanden.

## Idee

Suppoyo hilft allen, die in Krisenzeiten ihr Haus nicht verlassen können oder dürfen, auch weiterhin mit den wichtigsten Einkäufen versorgt zu bleiben. Wir verbinden lokale, engagierte Freiwillige mit Bedürftigen, um die Gefahr einer Ansteckung zu minimieren. Ein Fokus liegt hier vor allem auf Organisationen, die Bestellungen telefonisch sammeln und weiterverarbeiten müssen.

Ziel 1: Versorgen - Wer sein Haus nicht verlassen kann, muss zuverlässig versorgt werden.

Ziel 2: Vertrauen - Sicherheit durch verifizierte und schon bekannte Helfer.

Ziel 3: Verbinden - Eine Plattform, die Freiwillige und Bedürftige verbindet.

## Technologie

Wir haben uns dafür entschieden das JavaScript-Framework Angular für den Prototypen zu verwenden.

Im folgenden findest du weitere Details zum Arbeiten mit dieser Angular-Applikation (auf Englisch):

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.3.

### Installation

After cloning the repository, use `npm install` to install all required packages.

### Development server

Run `ng serve` or `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
