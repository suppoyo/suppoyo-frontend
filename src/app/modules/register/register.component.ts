import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
    selector: 'sup-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
    public get roles() {
        return Object.keys(Roles).map((x: keyof typeof Roles) => {
            return {key: x, value: Roles[x]};
        });
    }
    public showPassword = false;
    public showPasswordConfirm = false;
    public registerForm = new FormGroup({
        name: new FormControl('', Validators.pattern('([A-Z][a-z]{1,}), ([A-Z][a-z]{1,})')),
        password: new FormGroup(
            {
                password: new FormControl(null, Validators.required),
                confirmPass: new FormControl(null, Validators.required),
            },
            [this.checkPasswords]
        ),
        phone: new FormControl(''),
        address: new FormControl(''),
        mail: new FormControl('', Validators.email),
        role: new FormControl(null),
        notes: new FormControl(''),
    });
    constructor(private router: Router) {}

    ngOnInit(): void {}

    checkPasswords(group: FormGroup) {
        const pass = group.get('password').value;
        const confirmPass = group.get('confirmPass').value;

        return pass === confirmPass ? null : {notSame: true};
    }

    public register() {
        console.log(this.registerForm);
        this.router.navigate(['login']);
    }
}

export enum Roles {
    HELPER = 'Helfer:inn',
    IN_NEED_OF_HELP = 'Hilfsbedürftige:r',
}
