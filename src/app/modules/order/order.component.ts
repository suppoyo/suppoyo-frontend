import {Component, OnInit, ElementRef} from '@angular/core';
import {FormControl, FormGroup, Validators, FormBuilder, FormArray} from '@angular/forms';

@Component({
    selector: 'sup-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss'],
})
export class OrderComponent implements OnInit {
    public orderForm: FormGroup;
    constructor(private fb: FormBuilder) {
        this.orderForm = this.fb.group({
            price: this.fb.group({
                totalPriceFrom: ['', [Validators.min(1), Validators.max(99), Validators.required]],
                totalPriceTo: ['', [Validators.min(5), Validators.max(100), Validators.required]],
            }),
            products: this.fb.array([this.createFormGroup()]),
        });
    }

    ngOnInit(): void {}

    public createFormGroup(): FormGroup {
        return this.fb.group({
            product: ['', [Validators.required]],
            count: [1, [Validators.min(1), Validators.max(10), Validators.required]],
        });
    }

    public getProducts(): FormArray {
        return this.orderForm.get('products') as FormArray;
    }

    public addFormGroup() {
        const a = this.orderForm.get('products') as FormArray;
        (this.orderForm.get('products') as FormArray).push(this.createFormGroup());
        // console.log(a);
    }

    public getValue(i: number) {
        return this.orderForm.get('products')['controls'][i];
    }

    public increment(input: HTMLInputElement) {
        input.value = `${Number.parseInt(input.value, 10) + 1}`;
    }

    public decrement(input: HTMLInputElement) {
        input.value = `${Number.parseInt(input.value, 10) - 1}`;
    }
}
