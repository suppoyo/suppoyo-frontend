import {Component, OnInit, Input} from '@angular/core';

@Component({
    selector: 'sup-goal',
    templateUrl: './goal.component.html',
    styleUrls: ['./goal.component.scss'],
})
export class GoalComponent implements OnInit {
    @Input() public goalName: string;
    @Input() public goalDescription: string;
    @Input() public goalIcon: string;

    constructor() {}

    ngOnInit(): void {}
}
