import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ImprintComponent} from './imprint/imprint.component';
import {FooterComponent} from './footer/footer.component';
import {NavigationComponent} from './navigation/navigation.component';
import {RoleTileComponent} from './role-tile/role-tile.component';
import {GoalComponent} from './goal/goal.component';

@NgModule({
    declarations: [FooterComponent, GoalComponent, ImprintComponent, NavigationComponent, RoleTileComponent],
    imports: [CommonModule],
    exports: [NavigationComponent, GoalComponent, RoleTileComponent, FooterComponent],
})
export class SharedModule {}
