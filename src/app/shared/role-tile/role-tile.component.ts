import {Component, OnInit, Input, OnChanges} from '@angular/core';

@Component({
    selector: 'sup-role-tile',
    templateUrl: './role-tile.component.html',
    styleUrls: ['./role-tile.component.scss'],
})
export class RoleTileComponent implements OnChanges {
    @Input() public roleName: string;
    @Input() public roleClass: string;
    @Input() public roleDescription: string;
    @Input() public roleImage: string;

    constructor() {}
    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        console.log(changes);
    }
}
