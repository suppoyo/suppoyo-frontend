import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'sup-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit {
    public open = false;

    constructor() {}

    public ngOnInit(): void {}

    public toggleNav(event) {
        this.open = !this.open;
    }
}
