import {Router} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';

@Component({
    selector: 'sup-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
    public loginForm = new FormGroup({
        mail: new FormControl('', Validators.email),
        password: new FormControl(''),
    });

    constructor(private router: Router) {}

    ngOnInit(): void {}

    public login() {
        this.router.navigate(['onboarding']);
    }
}
