import {OrderComponent} from './modules/order/order.component';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './modules/register/register.component';
import {OnboardingComponent} from './modules/onboarding/onboarding.component';
import {ImprintComponent} from './shared/imprint/imprint.component';
import {StartComponent} from './modules/start/start.component';

const routes: Routes = [
    {
        path: '',
        component: StartComponent,
    },
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: 'order',
        component: OrderComponent,
    },
    {
        path: 'onboarding',
        component: OnboardingComponent,
    },
    {
        path: 'register',
        component: RegisterComponent,
    },
    {
        path: 'imprint',
        component: ImprintComponent,
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
