import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {SharedModule} from './shared/shared.module';
import {StartComponent} from './modules/start/start.component';
import {RegisterComponent} from './modules/register/register.component';
import {LoginComponent} from './login/login.component';
import { OnboardingComponent } from './modules/onboarding/onboarding.component';
import { OrderComponent } from './modules/order/order.component';

@NgModule({
    declarations: [AppComponent, StartComponent, RegisterComponent, LoginComponent, OnboardingComponent, OrderComponent],
    imports: [
        BrowserModule,
        AppRoutingModule,
        ReactiveFormsModule,
        SharedModule,
        ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
    ],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
